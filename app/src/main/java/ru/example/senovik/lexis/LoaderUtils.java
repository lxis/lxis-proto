package ru.example.senovik.lexis;

public class LoaderUtils {

    // Loader ids
    public static final int CARD_LIST_FRAGMENT_LOADER_ID = 200;
    public static final int CARD_DETAIL_FRAGMENT_LOADER_ID = 201;

    private LoaderUtils() {
        // no instances
    }

}
