package ru.example.senovik.lexis.tasks;

import android.content.Context;

import ru.example.senovik.lexis.data.DBCardContract;

/**
 * Created by senovik on 16.12.2015.
 */
public class DeleteCardTask implements Runnable {

    private Context mContext;
    private int mCardId;

    public DeleteCardTask(Context context, int cardId) {
        mContext = context;
        mCardId = cardId;
    }

    @Override
    public void run() {
        mContext.getContentResolver().delete(DBCardContract.CardEntry.CONTENT_URI,
                mCardId + " = " +
                        DBCardContract.CardEntry.COLUMN_ID, null);
    }
}
