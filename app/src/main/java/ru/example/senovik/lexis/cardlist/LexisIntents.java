
package ru.example.senovik.lexis.cardlist;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by sergei on 15.01.16.
 */
public class LexisIntents {

    public static final int RESULT_LOAD_IMAGE = 1;

    public static final String EXTRA_KEY_CARD_ID = "EXTRA_KEY_CARD_ID";

    public static final int ERROR_CARD_ID = -1;

    public static void openGalleryToSetImageForCardWithId(int id, Activity aActivity) {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        aActivity.getIntent().putExtra(EXTRA_KEY_CARD_ID, id);
        aActivity.startActivityForResult(intent, LexisIntents.RESULT_LOAD_IMAGE);
    }

    private LexisIntents() {
        //no instances
    }

}
