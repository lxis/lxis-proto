package ru.example.senovik.lexis.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DBCardContract {

    public static final String AUTHORITY = "ru.example.senovik.lexis";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_CARDS = "cards";

    public static final class CardEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CARDS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + AUTHORITY + "/" + PATH_CARDS;
        public static final String CONTENT_TYPE_ITEM =
                "vnd.android.cursor.dir/" + AUTHORITY + "/" + PATH_CARDS + "/*";

        // Table name
        public static final String TABLE_NAME = "cards";

        // Table columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_WORD = "word";
        public static final String COLUMN_TRANSLATION = "translation";
        public static final String COLUMN_IMG_URL = "img_url";
        public static final String COLUMN_CARD_TYPE = "card_type";

        // for probable future needs
        public static final String TYPE_SIMPLE_CARD = "simple_card";

    }
}
