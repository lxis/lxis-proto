package ru.example.senovik.lexis.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.example.senovik.lexis.data.DBCardContract.CardEntry;

class DBCardHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "cards.db";

    public DBCardHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_STORE_TABLE = "CREATE TABLE " + CardEntry.TABLE_NAME + " (" +
                CardEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CardEntry.COLUMN_WORD + " TEXT NOT NULL, " +
                CardEntry.COLUMN_TRANSLATION + " TEXT NOT NULL, " +
                CardEntry.COLUMN_IMG_URL + " TEXT NOT NULL, " +
                CardEntry.COLUMN_CARD_TYPE + " TEXT NOT NULL);";

        sqLiteDatabase.execSQL(SQL_CREATE_STORE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
    }

}
