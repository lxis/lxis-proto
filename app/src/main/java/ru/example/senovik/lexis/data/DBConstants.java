package ru.example.senovik.lexis.data;

public class DBConstants {

    public static final String[] CARDS_COLUMNS =
            {DBCardContract.CardEntry.TABLE_NAME + "." + DBCardContract.CardEntry._ID, DBCardContract.CardEntry.COLUMN_WORD,
                    DBCardContract.CardEntry.COLUMN_TRANSLATION, DBCardContract.CardEntry.COLUMN_IMG_URL,
                    DBCardContract.CardEntry.COLUMN_CARD_TYPE};

}
