package ru.example.senovik.lexis.cardlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.example.senovik.lexis.ExecutableQueue;
import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.tasks.AddCardTask;

public class NewCardFragment extends Fragment implements View.OnClickListener {

    private EditText mWord;
    private EditText mTranslation;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_card, container, false);

        mWord = (EditText) view.findViewById(R.id.new_word);
        mTranslation = (EditText) view.findViewById(R.id.new_translation);

        view.findViewById(R.id.new_save_button).setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.new_save_button) {
            ExecutableQueue.getInstance()
                    .execute(new AddCardTask(
                            getActivity(),
                            mWord.getText().toString(),
                            mTranslation.getText().toString())
                    );
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }


}
