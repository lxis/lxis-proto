package ru.example.senovik.lexis.cardlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.Map;

import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.cards.Card;

class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final Map<Integer, Card> mDataSet;
    private final ListScroller mListScroller;

    public CardAdapter(Context context, Map<Integer, Card> dataSet, ListScroller listScroller) {
        mContext = context;
        mDataSet = dataSet;
        mListScroller = listScroller;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_simple_card, parent, false);
        return new CardViewHolder(mContext, view, mListScroller);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CardViewHolder) {
            Card card = (Card) mDataSet.values().toArray()[position];
            CardViewHolder cardViewHolder = (CardViewHolder) holder;
            cardViewHolder.mWord.setText(card.getWord());
            cardViewHolder.mTranslation.setText(card.getTranslation());
            cardViewHolder.mCard = card;
            Glide.with(mContext).load(card.getImgURL()).into(cardViewHolder.mImage);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

}
