
package ru.example.senovik.lexis.tasks;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import ru.example.senovik.lexis.data.DBCardContract;

/**
 * Created by senovik on 16.12.2015.
 */
public class UpdateCardTask implements Runnable {

    private Context mContext;

    private int mCardId = -1;
    private String mWord;
    private String mTranslation;
    private String mImgUrl;

    public UpdateCardTask(Context context) {
        mContext = context;
    }

    @Override
    public void run() {
        Log.i("novikov", "UpdateCardTask run");

        if (mCardId == -1) {
            Log.i("novikov", "UpdateCardTask run return from mCardId == -1");
            return;
        }

        if (mImgUrl == null) {
            Log.i("novikov", "UpdateCardTask run mImgUrl == null");
            mImgUrl = "";
        }

        ContentValues cardValues = new ContentValues();

        if (mWord != null) {
            cardValues.put(DBCardContract.CardEntry.COLUMN_WORD, mWord);
        }
        if (mTranslation != null) {
            cardValues.put(DBCardContract.CardEntry.COLUMN_TRANSLATION, mTranslation);
        }

        cardValues.put(DBCardContract.CardEntry.COLUMN_IMG_URL, mImgUrl);
        cardValues.put(DBCardContract.CardEntry.COLUMN_CARD_TYPE, DBCardContract.CardEntry.TYPE_SIMPLE_CARD);

        mContext.getContentResolver().update(
                DBCardContract.CardEntry.CONTENT_URI,
                cardValues,
                mCardId + " = " +
                        DBCardContract.CardEntry.COLUMN_ID,
                null
                );
    }

    public void setCardId(int cardId) {
        mCardId = cardId;
    }

    public void setWord(String word) {
        mWord = word;
    }

    public void setTranslation(String translation) {
        mTranslation = translation;
    }

    public void setImgUrl(String imgUrl) {
        mImgUrl = imgUrl;
    }

}
