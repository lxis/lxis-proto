package ru.example.senovik.lexis.details;

import android.content.Context;

import ru.example.senovik.lexis.cards.Card;

/**
 * Factory class for TextWatcher objects.
 */
public class TextWatcherFactory {

    public static SavingTextWatcher newWordTextWatcher(Context context) {
        return newTextWatcher(context, SavingTextWatcher.DetailEdittextType.WORD);
    }

    public static SavingTextWatcher newTranslationTextWatcher(Context context) {
        return newTextWatcher(context, SavingTextWatcher.DetailEdittextType.TRANSLATION);
    }

    private static SavingTextWatcher newTextWatcher(Context context, SavingTextWatcher.DetailEdittextType type) {
        return new SavingTextWatcher(context, type);
    }

}
