package ru.example.senovik.lexis.details;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Toast;

import ru.example.senovik.lexis.ExecutableQueue;
import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.cards.Card;
import ru.example.senovik.lexis.tasks.UpdateCardTask;

/**
 * Saves changes in words and translation into database asynchronously.
 */
public class SavingTextWatcher implements TextWatcher {

    private static final long SAVING_DELAY_MILLIS = 2000;

    public enum DetailEdittextType {
        WORD, TRANSLATION;
    }

    private final Context mContext;

    private DetailEdittextType mDetailEdittextType;

    private Handler mHandler = new Handler();

    private UpdateCardTask mUpdateCardTask;

    private Runnable mSaveUpdatesOnBackgroundThreadTask = new Runnable() {
        @Override
        public void run() {
            ExecutableQueue.getInstance().execute(mUpdateCardTask);
        }
    };

    private Runnable mShowToastTask = new Runnable() {
        @Override
        public void run() {
            String saved = mContext.getString(R.string.card_saved);
            Toast.makeText(mContext, saved, Toast.LENGTH_SHORT).show();
        }
    };

    public SavingTextWatcher(Context context, DetailEdittextType detailEdittextType) {
        mContext = context;
        mDetailEdittextType = detailEdittextType;
        mUpdateCardTask = new UpdateCardTask(mContext);
    }

    @Override
    public void beforeTextChanged(CharSequence sequence, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence sequence, int start, int before, int count) {
        Log.i("novikov", "onTextChanged");

        mHandler.removeCallbacks(mSaveUpdatesOnBackgroundThreadTask);
        mHandler.removeCallbacks(mShowToastTask);

        switch (mDetailEdittextType) {
            case WORD:
                mUpdateCardTask.setWord(sequence.toString());
                break;
            case TRANSLATION:
                mUpdateCardTask.setTranslation(sequence.toString());
                break;
        }

        mHandler.postDelayed(mSaveUpdatesOnBackgroundThreadTask, SAVING_DELAY_MILLIS);
        mHandler.postDelayed(mShowToastTask, SAVING_DELAY_MILLIS);

    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    /**
     * Initiates mUpdateCardTask object with current card values.
     *
     * @param card
     */
    public void presetDataForUpdateCardTask(Card card) {
        Log.i("novikov", "watcher presetDataForUpdateCardTask");
        Log.i("novikov", "watcher card.getID(): " + card.getID());
        Log.i("novikov", "watcher card.getWord(): " + card.getWord());
        Log.i("novikov", "watcher card.getTranslation(): " + card.getTranslation());
        mUpdateCardTask.setCardId(card.getID());
        mUpdateCardTask.setWord(card.getWord());
        mUpdateCardTask.setTranslation(card.getTranslation());
        mUpdateCardTask.setImgUrl(card.getImgURL());
    }

}
