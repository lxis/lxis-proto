package ru.example.senovik.lexis.cardlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import ru.example.senovik.lexis.ExecutableQueue;
import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.tasks.DeleteCardTask;

/**
 * Created by senovik on 16.12.2015.
 */
public class PopupFactory {

    private Context mContext;

    public PopupFactory(Context context) {
        mContext = context;
    }

    public AlertDialog createInsertDialog(final int cardId) {
        return new AlertDialog.Builder(mContext).setMessage(mContext.getString(R.string.insert_message))
                .setPositiveButton(mContext.getString(R.string.gallery),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Activity activity;
                                try {
                                    activity = (Activity) mContext;
                                } catch (ClassCastException cce) {
                                    throw new ClassCastException("ViewHolder context is not an activity");
                                }
                                LexisIntents.openGalleryToSetImageForCardWithId(cardId, activity);
                            }
                        })
                .setNegativeButton(mContext.getString(R.string.google_drive),
                        null)
                .create();
    }

    public AlertDialog createDeleteDialog(final int cardId) {
        return new AlertDialog.Builder(mContext).setMessage(mContext.getString(R.string.delete_message))
                .setPositiveButton(mContext.getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ExecutableQueue.getInstance()
                                        .execute(new DeleteCardTask(mContext, cardId));
                            }
                        })
                .setNegativeButton(mContext.getString(R.string.cancel),
                        null)
                .create();
    }

}
