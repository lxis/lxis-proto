package ru.example.senovik.lexis.cardlist;

public interface FragmentStarter {

    void startFragment();

    void blurScreen();

}
