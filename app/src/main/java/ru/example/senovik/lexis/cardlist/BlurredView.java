package ru.example.senovik.lexis.cardlist;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class BlurredView extends View {

    private final int UNBLURRED_CIRCLE_RADIUS = 350;

    private OnClickListener mOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibility(GONE);
        }
    };

    private Paint mPaint = new Paint();
    private Path mPath = new Path();

    public BlurredView(Context context) {
        super(context);
        init();
    }

    public BlurredView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BlurredView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnClickListener(mOnClickListener);
    }

    protected void onDraw(Canvas canvas) {
        mPaint.setColor(Color.GREEN);
        mPaint.setAlpha(127);
        int width = getWidth();
        int height = getHeight();

        mPath.moveTo(0, 0);
        mPath.lineTo(0, height - UNBLURRED_CIRCLE_RADIUS);

        mPath.arcTo(new RectF(
                -UNBLURRED_CIRCLE_RADIUS,
                height - UNBLURRED_CIRCLE_RADIUS,
                UNBLURRED_CIRCLE_RADIUS,
                height + UNBLURRED_CIRCLE_RADIUS
        ), 270, 180);

        mPath.lineTo(0, height);
        mPath.lineTo(width, height);
        mPath.lineTo(width, 0);
        mPath.close();

        canvas.drawPath(mPath, mPaint);
    }

}
