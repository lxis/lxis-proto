package ru.example.senovik.lexis.cards;

public class Card {

    private int mID;
    private String mWord;
    private String mTranslation;
    private String mImgURL;

    public Card(int ID, String word, String translation, String imgURL) {
        mID = ID;
        mWord = word;
        mTranslation = translation;
        mImgURL = imgURL;
    }

    public int getID() {
        return mID;
    }

    public String getWord() {
        return mWord;
    }

    public String getTranslation() {
        return mTranslation;
    }

    public String getImgURL() {
        return mImgURL;
    }

}
