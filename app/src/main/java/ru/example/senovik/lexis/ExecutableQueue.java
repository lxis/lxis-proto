package ru.example.senovik.lexis;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecutableQueue {
    private static ThreadPoolExecutor mExecutor;
    private static ExecutableQueue mInstance;
    private final long KIPE_ALIVE_TIME = 0L;
    private final int CORE_POOL_SIZE = 3;
    private final int MAXIMUM_POOL_SIZE = 5;

    private ExecutableQueue() {
        mExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KIPE_ALIVE_TIME, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    public static synchronized ExecutableQueue getInstance() {
        if (mInstance == null) {
            mInstance = new ExecutableQueue();
        }
        return mInstance;
    }

    public void execute(Runnable task) {
        mExecutor.execute(task);
    }

    public void shutdown() {
        mExecutor.shutdownNow();
    }
}
