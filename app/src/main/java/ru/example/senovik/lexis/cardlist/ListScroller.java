package ru.example.senovik.lexis.cardlist;

public interface ListScroller {

    void scroll(int positionInList);

}
