
package ru.example.senovik.lexis.cardlist;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import ru.example.senovik.lexis.ExecutableQueue;
import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.details.CardDetailActivity;
import ru.example.senovik.lexis.tasks.UpdateCardTask;

public class LexisActivity extends FragmentActivity {

    private final FragmentStarter mFragmentStarter = new FragmentStarter() {

        @Override
        public void startFragment() {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container_main, new NewCardFragment())
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void blurScreen() {
            mBlurredLayout.setVisibility(View.VISIBLE);
        }

    };

    private final DetailOpener mDetailOpener = new DetailOpener() {
        @Override
        public void openDetailActivity(int id) {
            Intent intent = new Intent(LexisActivity.this, CardDetailActivity.class);
            intent.putExtra(CardDetailActivity.EXTRA_CARD_ID, id);
            startActivity(intent);
        }
    };

    private BlurredView mBlurredLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lexis);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container_main, new CardListFragment())
                    .commit();
        }

        mBlurredLayout = (BlurredView) findViewById(R.id.blurred_layout);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LexisIntents.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {
                    MediaStore.Images.Media.DATA
            };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);

            int cardId = getIntent().getIntExtra(LexisIntents.EXTRA_KEY_CARD_ID, LexisIntents.ERROR_CARD_ID);
            if (cardId == LexisIntents.ERROR_CARD_ID) {
                throw new RuntimeException("LexisActivity::onActivityResult - incorrect card id");
            }

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                Log.i("novikov", "picturePath: " + picturePath);
                Log.i("novikov", "cardId: " + cardId);

                UpdateCardTask task = new UpdateCardTask(this);
                task.setCardId(cardId);
                task.setImgUrl(picturePath);
                ExecutableQueue.getInstance().execute(task);

            }

        }

    }

    public DetailOpener getDetailOpener() {
        return mDetailOpener;
    }

    public FragmentStarter getFragmentStarter() {
        return mFragmentStarter;
    }

}
