package ru.example.senovik.lexis.cardlist;

public interface DetailOpener {

    void openDetailActivity(int id);

}
