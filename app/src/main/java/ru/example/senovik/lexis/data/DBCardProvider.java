
package ru.example.senovik.lexis.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import ru.example.senovik.lexis.data.DBCardContract.CardEntry;

public class DBCardProvider extends ContentProvider {

    private static final int CARD_LIST = 1;
    private static final int CARD_ID = 2;
    private static final UriMatcher URI_MATCHER;

    private DBCardHelper mHelper;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(DBCardContract.AUTHORITY, DBCardContract.PATH_CARDS, CARD_LIST);
        URI_MATCHER.addURI(DBCardContract.AUTHORITY, DBCardContract.PATH_CARDS + "/#", CARD_ID);
    }

    @Override
    public boolean onCreate() {
        mHelper = new DBCardHelper(getContext());
        return false;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case CARD_LIST:
                return CardEntry.CONTENT_TYPE;
            case CARD_ID:
                return CardEntry.CONTENT_TYPE_ITEM;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {

        SQLiteDatabase db = mHelper.getWritableDatabase();

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(CardEntry.TABLE_NAME);

        switch (URI_MATCHER.match(uri)) {
            case CARD_ID:
                String rowID = uri.getPathSegments().get(1);
                queryBuilder.appendWhere(CardEntry.COLUMN_ID + " = " + rowID);
            default:
                break;
        }

        Cursor cursor = queryBuilder
                .query(db, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;

    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = mHelper.getWritableDatabase();

        long id = db.insert(DBCardContract.CardEntry.TABLE_NAME, null, values);
        if (id > -1) {
            Uri insertedId = ContentUris.withAppendedId(CardEntry.CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(insertedId, null);
            return insertedId;
        } else {
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase db = mHelper.getWritableDatabase();

        switch (URI_MATCHER.match(uri)) {
            case CARD_ID:
                String rowID = uri.getPathSegments().get(1);
                selection = DBCardContract.CardEntry.COLUMN_ID + " = " + rowID +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
            default:
                break;
        }

        if (selection == null) {
            selection = "1";
        }

        int deleteCount = db.delete(DBCardContract.CardEntry.TABLE_NAME, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);

        return deleteCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        Log.i("novikov", "Provider update");
        Log.i("novikov", "values w: " + values.getAsString(DBCardContract.CardEntry.COLUMN_WORD));
        Log.i("novikov", "values t: " + values.getAsString(CardEntry.COLUMN_TRANSLATION));
        Log.i("novikov", "values url: " + values.getAsString(CardEntry.COLUMN_IMG_URL));
        Log.i("novikov", "values ct: " + values.getAsString(CardEntry.COLUMN_CARD_TYPE));
        Log.i("novikov", "selection: " + selection);

        SQLiteDatabase db = mHelper.getWritableDatabase();

        switch (URI_MATCHER.match(uri)) {
            case CARD_ID:
                String rowID = uri.getPathSegments().get(1);
                selection = CardEntry.COLUMN_ID + " = " + rowID +
                        (!TextUtils.isEmpty(selection) ? "AND (" + selection + ')' : "");
            default:
                break;
        }

        int updateCount;

        // There are two basic case when update is used

        Log.i("novikov", "val: " + values.getAsString(CardEntry.COLUMN_WORD));
        Log.i("novikov", "translation: " + values.getAsString(CardEntry.COLUMN_TRANSLATION));
        if (values.containsKey(CardEntry.COLUMN_WORD)
                && values.containsKey(CardEntry.COLUMN_TRANSLATION)) {
            // The first one is typo correcting in card details,
            // In this case word and translation are not null and
            // SQLiteDatabase::updateWithOnConflict method is used to completely
            // replace a row in the database
            updateCount = db.updateWithOnConflict(
                    CardEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs,
                    SQLiteDatabase.CONFLICT_IGNORE
                    );
        } else {
            // The second one is img_url updating, when a new image
            // is selected for a card, here full replacing is
            // unnecessary and SQLiteDatabase::update method is used to change
            // img_url column only
            Log.i("novikov", "val: " + values.getAsString(CardEntry.COLUMN_WORD));
            updateCount = db.update(
                    CardEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs
                    );
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return updateCount;
    }

}
