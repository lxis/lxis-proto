package ru.example.senovik.lexis.tasks;

import android.content.ContentValues;
import android.content.Context;

import ru.example.senovik.lexis.data.DBCardContract;

/**
 * Created by senovik on 16.12.2015.
 */
public class AddCardTask implements Runnable {

    private Context mContext;

    private final String mWord;
    private final String mTranslation;

    public AddCardTask(Context context, String word, String translation) {
        mContext = context;
        mWord = word.trim();
        mTranslation = translation.trim();
    }

    @Override
    public void run() {
        ContentValues cardValues = new ContentValues();

        cardValues.put(DBCardContract.CardEntry.COLUMN_WORD, mWord);
        cardValues.put(DBCardContract.CardEntry.COLUMN_TRANSLATION, mTranslation);
        cardValues.put(DBCardContract.CardEntry.COLUMN_IMG_URL, "");
        cardValues.put(DBCardContract.CardEntry.COLUMN_CARD_TYPE, DBCardContract.CardEntry.TYPE_SIMPLE_CARD);

        mContext.getContentResolver().insert(DBCardContract.CardEntry.CONTENT_URI, cardValues);
    }

}
