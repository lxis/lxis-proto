package ru.example.senovik.lexis.cardlist;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.example.senovik.lexis.LoaderUtils;
import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.cards.Card;
import ru.example.senovik.lexis.data.DBCardContract;
import ru.example.senovik.lexis.data.DBConstants;

import java.util.HashMap;
import java.util.Map;

public class CardListFragment extends Fragment implements ListScroller {

    private final View.OnClickListener mOnFABClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mFragmentStarter.startFragment();
        }
    };

    private final LoaderManager.LoaderCallbacks<Cursor> mLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String sortOrder = DBCardContract.CardEntry.COLUMN_ID + " ASC";
            Uri cardUri = DBCardContract.CardEntry.CONTENT_URI;
            return new CursorLoader(getActivity(), cardUri, DBConstants.CARDS_COLUMNS, null, null, sortOrder);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mDataSet.clear();
            if (data != null && data.moveToFirst()) {
                do {
                    int id = data.getInt(data.getColumnIndex(DBCardContract.CardEntry.COLUMN_ID));
                    String word =
                            data.getString(data.getColumnIndex(DBCardContract.CardEntry.COLUMN_WORD));
                    String translation = data.getString(
                            data.getColumnIndex(DBCardContract.CardEntry.COLUMN_TRANSLATION));
                    String imgURL = data.getString(
                            data.getColumnIndex(DBCardContract.CardEntry.COLUMN_IMG_URL));
                    Card card = new Card(id, word, translation, imgURL);
                    mDataSet.put(card.getID(), card);
                } while (data.moveToNext());
            }
            mCardAdapter.notifyDataSetChanged();
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    private CardAdapter mCardAdapter;
    private FragmentStarter mFragmentStarter;

    private final Map<Integer, Card> mDataSet = new HashMap<>();

    private RecyclerView mRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mFragmentStarter = ((LexisActivity) context).getFragmentStarter();
        } catch (ClassCastException cce) {
            throw new ClassCastException("CardListFragment onAttach cast error");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_card_list, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(getActivity()
                .getResources().getColor(R.color.toolbar_title_color));

        view.findViewById(R.id.fabButton).setOnClickListener(mOnFABClickedListener);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_all_cards);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCardAdapter = new CardAdapter(getActivity(), mDataSet, this);
        mRecyclerView.setAdapter(mCardAdapter);

        getLoaderManager().initLoader(LoaderUtils.CARD_LIST_FRAGMENT_LOADER_ID, null, mLoaderCallbacks);

        return view;

    }

    @Override
    public void scroll(int positionInList) {

        Log.i("novikov", "positionInList: " + positionInList);

        RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            ((LinearLayoutManager) layoutManager).scrollToPositionWithOffset(positionInList, 0);
        }
    }

}
