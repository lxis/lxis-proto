
package ru.example.senovik.lexis.cardlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.cards.Card;

class CardViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;

    private ListScroller mListScroller;

    private final DetailOpener mDetailOpener;
    private final FragmentStarter mFragmentStarter;

    final ImageView mImage;

    final TextView mWord;
    final TextView mTranslation;
    final ImageButton mCardButton;
    final ImageButton mEditButton;
    final ImageButton mInsertButton;
    final ImageButton mDeleteButton;
    Card mCard;

    public CardViewHolder(Context context, View convertView, ListScroller listScroller) {
        super(convertView);

        mContext = context;

        mListScroller = listScroller;

        try {
            mDetailOpener = ((LexisActivity) context).getDetailOpener();
            mFragmentStarter = ((LexisActivity) context).getFragmentStarter();
        } catch (ClassCastException cce) {
            throw new ClassCastException("ClassCastException in CardViewHolder constructor");
        }

        mImage = (ImageView) convertView.findViewById(R.id.image);
        mWord = (TextView) convertView.findViewById(R.id.word);
        mTranslation = (TextView) convertView.findViewById(R.id.translation);
        mEditButton = (ImageButton) convertView.findViewById(R.id.edit_button);
        mInsertButton = (ImageButton) convertView.findViewById(R.id.insert_button);
        mDeleteButton = (ImageButton) convertView.findViewById(R.id.delete_button);
        mCardButton = (ImageButton) convertView.findViewById(R.id.card_button);

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mDetailOpener != null) {
                    mDetailOpener.openDetailActivity(mCard.getID());
                }
            }

        });

        mCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListScroller.scroll(getAdapterPosition());

                int[] location = new int[2];
                mCardButton.getLocationOnScreen(location);
                int y = location[1];
                //                mFragmentStarter.blurScreen();

                boolean gone = mEditButton.getVisibility() == View.GONE;
                mEditButton.setVisibility(gone ? View.VISIBLE : View.GONE);
                mInsertButton.setVisibility(gone ? View.VISIBLE : View.GONE);
                mDeleteButton.setVisibility(gone ? View.VISIBLE : View.GONE);
            }
        });

        mInsertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopupFactory(mContext).createInsertDialog(mCard.getID()).show();
            }
        });

        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopupFactory(mContext).createDeleteDialog(mCard.getID()).show();
            }
        });
    }

}
