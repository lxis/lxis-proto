package ru.example.senovik.lexis.details;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.Map;

import ru.example.senovik.lexis.LoaderUtils;
import ru.example.senovik.lexis.R;
import ru.example.senovik.lexis.cards.Card;
import ru.example.senovik.lexis.data.DBCardContract;
import ru.example.senovik.lexis.data.DBConstants;

public class CardDetailActivity extends FragmentActivity {

    public static final String EXTRA_CARD_ID = "EXTRA_CARD_ID";

    private boolean mFirstLoadFinished = true;

    private final LoaderManager.LoaderCallbacks<Cursor> mLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String sortOrder = DBCardContract.CardEntry.COLUMN_ID + " ASC";
            Uri cardUri = DBCardContract.CardEntry.CONTENT_URI;
            return new CursorLoader(
                    CardDetailActivity.this,
                    cardUri,
                    DBConstants.CARDS_COLUMNS,
                    null,
                    null,
                    sortOrder);
        }

        @Override
        public void onLoadFinished(Loader loader, Cursor data) {
            mDataSet.clear();
            if (data != null && data.moveToFirst()) {
                do {
                    int id = data.getInt(data.getColumnIndex(DBCardContract.CardEntry.COLUMN_ID));
                    String word =
                            data.getString(data.getColumnIndex(DBCardContract.CardEntry.COLUMN_WORD));
                    String translation = data.getString(
                            data.getColumnIndex(DBCardContract.CardEntry.COLUMN_TRANSLATION));
                    String imgURL = data.getString(
                            data.getColumnIndex(DBCardContract.CardEntry.COLUMN_IMG_URL));
                    Card card = new Card(id, word, translation, imgURL);
                    mDataSet.put(card.getID(), card);
                } while (data.moveToNext());
            }

            int cardId = getIntent().getIntExtra(EXTRA_CARD_ID, -1);
            mCard = mDataSet.get(cardId);

            if (mFirstLoadFinished) {
                updateUI();
                mFirstLoadFinished = false;
            }
            updateWatchers();
        }

        @Override
        public void onLoaderReset(Loader loader) {

        }
    };

    private View.OnClickListener mCloseClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private Card mCard;

    private ImageView mCardImage;
    private EditText mWord;
    private EditText mTranslation;
    private final Map<Integer, Card> mDataSet = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mCardImage = (ImageView) findViewById(R.id.detail_card_image);
        mWord = (EditText) findViewById(R.id.detail_word);
        mTranslation = (EditText) findViewById(R.id.detail_translation);
        findViewById(R.id.detail_close_button).setOnClickListener(mCloseClickListener);

        getSupportLoaderManager().initLoader(LoaderUtils.CARD_DETAIL_FRAGMENT_LOADER_ID,
                null, mLoaderCallbacks);

    }

    public void updateUI() {
        if (mDataSet != null && mDataSet.size() > 0) {

            if (mCard != null) {
                String imgUrl = mCard.getImgURL();
                if (!TextUtils.isEmpty(imgUrl)) {
                    mCardImage.setVisibility(View.VISIBLE);
                    Glide.with(this).load(imgUrl).into(mCardImage);
                }

                String word = mCard.getWord();
                String translation = mCard.getTranslation();

                mWord.setText(word);
                mTranslation.setText(translation);

                Log.i("novikov", "cardId: " + mCard.getID());

            }
        }
    }

    private SavingTextWatcher mWordSavingTextWatcher
            = TextWatcherFactory.newWordTextWatcher(this);

    private SavingTextWatcher mTranslationSavingTextWatcher
            = TextWatcherFactory.newTranslationTextWatcher(this);

    private void updateWatchers() {
        mWord.removeTextChangedListener(mWordSavingTextWatcher);
        mWord.removeTextChangedListener(mTranslationSavingTextWatcher);

        mWordSavingTextWatcher.presetDataForUpdateCardTask(mCard);
        mTranslationSavingTextWatcher.presetDataForUpdateCardTask(mCard);

        mWord.addTextChangedListener(mWordSavingTextWatcher);
        mTranslation.addTextChangedListener(mTranslationSavingTextWatcher);
    }

}
